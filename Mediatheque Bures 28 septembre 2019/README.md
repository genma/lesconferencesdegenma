# Conférence L’usage de la cryptographie dans le cadre de l’hygiène numérique

La médiathèque de Bures sur Yvette, Essonne 91, organisait, pour la Fête de la science 2019, du 15 septembre au 15 octobre, un mois sur la cryptographie. Je suis intervenu pour cet événement pour donner une conférence le samedi 28 septembre à 16h00 intitulé : L’usage de la cryptographie dans le cadre de l’hygiène numérique. 

Le support est disponible dans ce dépôt.

# Commandes pour créer un pdf à partir des sources

```
#!/bin/bash
pandoc -st beamer -V theme:Custom -V lang:fr-FR Genma_Usage_crypto.md -o Genma_Usage_crypto.pdf --slide-level=2 --dpi=300
```

# Licence

CC-BY-SA
