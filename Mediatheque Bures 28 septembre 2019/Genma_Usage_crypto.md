% L'usage de la cryptographie dans le cadre de l'hygiène numérique
% Médiathèque de Bures-sur-Yvette
% Genma - 28/09/2019

## 

![](./Capture du 2019-09-20 15-22-14.png)

## A propos de moi

![](./images/avatar.png)

Présence sur Internet :

* *Le blog de Genma* https://blog.genma.fr
* Twitter : @genma
* Mastodon : genma@framasphere.org

Membre de **Framasoft** (*Degooglisons Internet*, Chatons...).

J'ai co-animé *des Cafés vie privée*.

Vulgarisateur et conférencier autour de l'hygiène numérique...

# Hygiène numérique ?

## Hygiène numérique ?

### Une définition?

* L'hygiène est un ensemble de mesures destinées à prévenir les infections et l'apparition de maladies infectieuses.
* L'hygiène numérique, ce sont des règles destinées à mieux utiliser son ordinateur, en sécurité, de façon simple.
* L'hygiène numérique c'est **comment éviter la gastro-informatique**.

# Chiffrement ? Crypté ? Cryptage ?

## Définitions - cryptage, crypter, chiffrement ?

### Le chiffrement

Le chiffrement consiste à chiffrer un document/un fichier à l'aide d'une clef de chiffrement. 

L'opération inverse étant le déchiffrement.

### Le cryptage

Le terme de cryptage est un anglicisme, tiré de l'anglais encryption. Le décryptage existe : il s'agit de "casser" un document chiffré lorsqu'on n'en a pas la clef.

### La cryptographie

La science quant-à elle s'appelle la "cryptographie".

## En résumé, pour se rappeler

![](./images/chiffrees_vs_cryptees.png)

# Le chiffrement, comment ça se passe ?

## Le chiffrement

###  Existe depuis l'antiquité

De tout temps l'Homme a voulu protéger ses secrets surtout en tant de guerre.

### Différents types de chiffrements

* Le chiffrement symétrique
* Le chiffrement asymétrique

### Avant tout des mathématiques

Dans tous les cas, ce sont des *opérations* / formules mathématiques qui sont utilisées.

## Le chiffrement symétrique

Cela consiste à chiffrer un message avec la même clef que celle qui sera utilisé pour le déchiffrement.

### Exemple : le code de César 

Consiste en un décalage de lettres. A->C, B->D etc.

Nous venons en paix -> Pqwu xgpqpu gp rckz

On applique le processus inverse pour avoir le message.

## Clef et algorithme de chiffrement

### Une clef de chiffrement, c'est quoi ?

Une clef s'appelle *une clef* car elle ouvre/ferme le cadenas qu'est *l'algorithme* de chiffrement utilisé.

* Dans l'exemple du chiffrement de César, l'algorithme est dans la notion de décalage.
* La clef est le nombre de lettre décallées (deux lettres dans l'exemple précédent).

### L'algorithme de chiffrement 

L'algorithme de chiffrement  est bien plus complexe que le fait de décaler des lettres ; il repose sur des notions mathématiques (nombre
premiers...)

## Le chiffrement  asymétrique 1/2

### Clef publique - clef privée

Le chiffrement asymétrique repose sur un couple de clef publique - clef privée.

Ce qu'il faut comprendre/retenir :

* Ma clef privée est secrète.
* Ma clef publique est distribuée à tous.

## Le chiffrement  asymétrique 2/2

![](./images/Alice_et_bob.png)

## Le chiffrement 

### Chiffrement

Avec la clef publique de mon correspondant, je chiffre un fichier.

### Conséquence

Le fichier ne peut plus être déchiffré que par la personne qui possède la clef privée correspondant à la clef publique que j'ai
utilisée (donc mon correspondant).

## Le déchiffrement 

### Déchiffrement

Avec sa clef privée, mon correspondant déchiffre le fichier.

### Conséquence

Il peut alors lire le message.

## Décryptage

Rappel :  il s'agit de "casser" un document chiffré lorsqu'on n'en a pas la clef.

On teste toutes les combinaisons possibles (pour faire simple).

# Le stockage des mots de passe

## Le stockage des mots de passe

### Le stockage des mots de passe

Pour le stockage des mots de passe, on ajoute une notion de sel cryptographique.

En effet, on a des dizaines de combinaisons de la forme

`fonction(mot de passe, clef) -> Mot de passe chiffré`

La fonction est souvent connue, les mots de passe sont souvent les mêmes.

## Dictionnaire de mot de passe

### Rainbow table

Il existe des dictionnaires de mot de passes

Et on génère des tables arc-en-ciel ou rainbow table : toutes les combinaisons possibles de mots de passe avec des clefs différentes pour des des fonctions connues.

## Chiffré salé

On ajoute un sel cryptographique (un code unique).

`fonction(mot de passe, clef, sel cryptographique) -> Mot de passe chiffré`

Donc pour avoir toutes les combinaisons possibles, il faut faire BEAUCOUP plus d'opérations....

# Les limites du chiffrement

## Attention à qui possède la clef

On échange avec des cadenas, mais qui a un double de la clef ?

* Echange en Https : le serveur
* Echange via messagerie chiffré : si il y a un intermédiaire...

## Les limites du chiffrement

* Ce qui est chiffré aujourd'hui pourra être déchiffré demain.
   * Les ordinateurs de demain pourront permettre de décrypter les données chiffrées aujourd'hui.

**Si on perd la clef on n'a plus accès aux données.**

# Les polémiques autour du chiffrement

## Est-ce que c'est légal ? 1/2

### Le chiffrement et la loi 1/2

En France, la loi considère donc que l'utilisation de moyens de cryptologie est libre (LCEN article 30-1) et il n'y a donc, actuellement
pas de limite à la taille de la clef de chiffrement que l'on peut utiliser.

En cas de perquisition,le refus de remise de la clef de chiffrement peut entraîner 3 ans d'emprisonnement ainsi que 45.000 eurs d'amende.

## Est-ce que c'est légal ? 2/2

### Le chiffrement et la loi 2/2

Cette peine est aggravée dans le cas où le chiffrement a été utilisé pour commettre un délit.

Il est donc recommandé de donner la clef de déchiffrement, sauf dans le cas où les données déchiffrées entrainerait une procédure judiciaire dont la peine finale serait supérieure à celle de l'entrave à l'enquête judiciaire.

## Polémiques autres

### Le chiffement est utilisé par les terroristes etc.

Et empêche le travail de la police.

Un élément de réponse : Ce n'est pas l'outil qui est en cause mais l'usage qu'on en fait. Interdit-on les voitures ?

### Les ransongiciels / ransonwares

Les fameux cryptolockers...

Amène la notion d'hygiène numérique.

# Et le rapport avec l'hygiène numérique ?

## Quel rapport entre le chiffrement et l'hygiène numérique

![](./images/hygiene.png)
 
* L'hygiène est un ensemble de mesures destinées à prévenir les infections et l'apparition de maladies infectieuses.
* L'hygiène numérique, ce sont des règles destinées à mieux utiliser son ordinateur, en sécurité, de façon simple.
* L'hygiène numérique c'est **comment éviter la gastro-informatique**.

# L'usage du chiffrement pour protéger ses données personnelles.

# Quoi et comment chiffer ?

## On me vole mon PC

* Quelles sont les données que je perds ? Amène la notion de
sauvegarde.
* Quelles sont les données que l'on trouve ? Amène la notion de chiffrement, de coffre-fort numérique.

![](./images/vol_pc.png)

## Quoi et comment chiffrer ?

### En local (sur sa ordinateur) - ses données

* Son disque dur (Bitlocker pour Windows, FileVault pour Mac)
* Sa clef USB
* Son smartphone (Option dans les paramètres)

### En réseau - ses communications

* Sa connexion Internet 
   * Https : le fameux cadenas,
   * la clef "WIFI",
   * utiliser un VPN...
* Les messages instantanés : What's App, Signal, Telegram...
* Ses e-mails

## Le logiciel libre

### Quelques rappels (ou présentation) sur le logiciel libre - l'opensource :

* le code source accessible
* il y a la possibilité de modifier et adapter
* il y a la possibilité redistribuer le logiciel

### Pourquoi le logiciel libre ?

* Sécurité
* Confidentialité des données personnelles
* ...

## Des coffres-fort numériques

Il existe différents logiciels pour protéger ses données

![](./images/coffre_fort_numerique.png)

## Pour les mots de passe

![](./images/keepassXc.png)


## Et les données dans le cloud ?

![](./images/cloud_data_center.jpg)

## Rappel sur ce qu'est le cloud

Les données stockées doivent être chiffrées sur des machines de confiance, et on y accède de façon chiffrée (https) avec des mots de passes sûr et de qualités.

![](./images/cloud.png)

# Conclusion

## Conclusions

* Formez vous à l'hygiène numérique, parlez en autour de vous
* Utilisez du logiciel libre
* Utilisez le chiffrement (et vous le faîtes déjà)

# Quizz / Test

## Un petit quizz / test

* Doit-on dire chiffer, crypter ? ;)
* Est-ce que le cadenas vert est important quand je suis sur Internet ?
* J'ai oublié mon mot de passe, je clique et je reçois un mail avec mon mot de passe : votre avis ?
* Est-ce que c'est important de chiffrer ses appareils ?
 
# Merci de votre attention & place aux questions & démonstrations
