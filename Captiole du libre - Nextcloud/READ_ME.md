# Nextcloud - De l'usage personnel sur Yunohost à un usage professionnel chez Atos

Facile à installer, facile à gérer, pourquoi ne pas héberger votre propre Nextcloud sur YunoHost (Yunohost est un système d'exploitation serveur destiné à rendre l'auto-hébergement accessible au plus grand nombre, sans nuire à la qualité et à la fiabilité du logiciel). Mais Nextcloud, ce n'est pas que pour les particuliers. Au sein du centre OpenSource d'Atos, je travaille au développement d’une offre et d’un partenariat autour de Nextcloud & Collabora pour nos clients. Petite présentation d'en quoi Nextcloud, c'est la solution cloud qui peut répondre aux besoins professionnel en alternative à Office365 ou GoogleDocs.

Genma, membre de différentes communautés du logiciel libre, blogueur, conférencier, j'interviendrai dans le cadre professionnel en qualité de chef de projet - Architecte opensource de l'AOSC, Atos Open Source Center.
