% Nextcloud - Du cloud personnel au professionnel
% Genma - Capitole du libre 2019 Toulouse
% De Yunohost à Atos

## A propos de moi - Genma

![Mon avatar](./images/avatar.png)

* **Un blog :** https://blog.genma.fr
* **Twitter :** @genma
* **Mastodon :** genma@framasphere.org
* **Membre de Framasoft** (*Degooglisons Internet*, Chatons...)
* **Utilisateur de logiciel libre depuis 2003**.
* **Yunohost && Nextcloud *Evangelist***

## Et professionnellement

### Avant

J'ai déjà parlé lors des éditions antérieures du Captiole du libre de mes expériences précédentes ;)

### Actuellement

* Je travaille pour **Atos** comme Chef de projet - Architecte et Spécialiste Open source.

# Yunohost

## Yunohost

* Un système d'exploitation pour bâtir un internet décentralisé
  * La Brique Internet utilise Yunohost comme O.S.

Une sorte **"d'Ubuntu pour l'auto-hébergement"**

**https://yunohost.org**

## Yunohost - quelques fonctionnalités

**Yunohost les basiques :**

* Basé sur Debian (stable, robuste...)
* Une interface d'administration web simple
* Installation des applications en quelques clics et questions
* Multi-utilisateurs avec un portail SSO
* Des Emails et un serveur XMPP *out of the box*
* Intégration de Let's Encrypt
* Firewall - Pare-feu, fail2ban, ...

**Et plein d'autres choses qui *juste* marchent**.

## Yunohost - Architecture

![](./images/Yunohost_Architecture.png)

## Yunohost - interface de connexion

![](./images/Yunohost_login.png)

## Yunohost - interface utilisateur

![Yunohost -](./images/Yunoshost_User_apps.png)

## Yunohost - Web admin interface

![](./images/Yunohost_Interface_admin.png)
 
## Yunohost - Web vs la ligne de commande

![](./images/Yunohost_Webinterface_vs_command_line.png)

## Yunohost - les applications sont packagées

Toutes les applications sont "packagées" et disponible sur un *repository* Github .
* Les scripts shell sont des *"moulinettes"* Yunohost.

![](./images/Yunohost_Apps_Package.png)

# Nextcloud & Yunohost

## Nextcloud & Yunohost

* Installer Nextcloud ... en quelques clics !
* Comme beaucoup d'applications, Nextcloud est packagée 
   * https://github.com/YunoHost-Apps/nextcloud_ynh

## Nextcloud & Yunohost - En arrière plan, un script shell

![](./images/Yunohost_Script_install.png)

## Nextcloud & Yunohost - Installation graphique

![](./images/Yunohost_Nextcloud_Installation_01.png)

## Nextcloud & Yunohost - Installation graphique

![](./images/Yunohost_Nextcloud_Installation_02.png)

## Nextcloud & Yunohost

![](./images/Yunohost_Nextcloud02.png)

# Interessé ? Convaincu ?

## Comment aider & participer à Yunohost ?

* Déployer une instance YunoHost pour vous-même ;)
* Parlez-en à vos amis, organiser des *install parties*
* Faites nous des retours sur l'interface...
* Faites des rapports de bugs, testez les applications et les versions beta...
* Améliorer la traduction et la documentation...

... et si vous savez développer :

* Front-end (HTML / CSS / JS)
* App packaging (bash)
* Core (python, general sysadmin knowledge, security, ...)

**https://yunohost.org**

# Atos

## Atos

### Une ESN française mais aussi internationale

Basée à Bezons (95), en France, avec des bureaux dans le monde entier.

* Une E.S.N. spécialisée dans:
   * le cloud,
   * le Big Data
   * la cybersécurité,
   * ...
   
Atos est présent dans le monde entier sous les marques Atos, Atos-Syntel, Atos Consulting, Atos Healthcare, Atos Worldgrid, Bull, Canopy, Unify et Worldline.

## AOSC

### Atos Open Source Center

* Un centre d'expertise et de support pour les logiciels Open Source (Red Hat et communautaire) par Atos.
* Nous sommes partenaire N°1 de Red Hat en Europe.

## Notre offre de service

### Consulting: conseil et expertise

* Évaluer l'opportunité et / ou les impacts de l'utilisation ou de la migration vers l'Open source et faciliter le choix des composants Open source.
  * De l'assistance à la mise en œuvre.

### Support

* Veille et conseils à nos clients sur les logiciels Opensource
* Hotline / Helpdesk
* Support, correction de bugs

## Offres et services

### Devops Factory

* Une *usine devops* basée sur des logiciel Opensource pour nos besoins internes et les besoins de nos clients.

### Transformation *digitale*

* Micro-services architecture
* Iaas, Paas, Saas
* Cloud : privé, hybdride, publique

# Atos & Nextcloud ?

# Pourquoi le centre Open Source d’Atos développe-t-il un modèle d’entreprise autour de Nextcloud?

## A cause (grâce) à moi !

Comme dit en introduction, je suis un *spécialiste de l'Open source* mais avant tout un *Nextclouders*.

Je crois *vraiment* (et sait que c'est vrai) que Nextcloud est une réponse aux GAFAM et BATX (cloud chinois). Et pas seulement moi !

## La popularité de Nextcloud augmente et n'est plus à démontrer !

### Les gouvernements européens choisissent l'indépendance des fournisseurs de cloud américains avec Nextcloud

* Le ministère français de l'Intérieur prépare actuellement le déploiement d'un Nextcloud prêt à la production pour ses utilisateurs, conçu pour s'adapter aux 300.000 employés du ministère.

https://nextcloud.com/blog/eu-governments-choose-independence-from-us-cloud-providers-with-nextcloud/

# Et donc pourquoi, Atos ?

## Parce-que (Orangina rouge mode).

Atos est une entreprise **TRES** grande.

* Nous avons plusieurs milliers de collaborateurs de toutes les expertises.
* Nous sommes en mesure de répondre à tous les besoins de nos clients.

## Qu'est-ce que nous prévoyons de faire avec Nextcloud?

### Faire ce que nous savons déjà faire avec l'Open source

* Convaincre nos clients d’utiliser Nextcloud comme alternative à leurs *besoins de type cloud*.
* Installez Nextcloud sur leurs infrastructures ou héberger pour leurs comptes, les serveurs Nexctloud sur notre offre de cloud / datacenter.
* Leurs fournir du support.

** Créons une offre Nextcloud pour nos clients !**

## Avantages pour Nextcloud?

* Plus de clients et d'utilisateurs
* Rapports de bugs
* Contributions
   * Nouvelles applications
   * Amélioration des applications existantes

# La suite ?

## La suite ?

### Trouver des clients

* Terminer pour construire l'offre
* ... et la communication à ce sujet

### Partenariat

* Avec Nextcloud
* Avec Collabora
* Avec Onlyoffice

### Meetup

Nous prévoyons d'organiser un Nextcloud Meetup à Paris, en France.

Statut : Work In Progress

# Merci de votre attention, place aux questions
