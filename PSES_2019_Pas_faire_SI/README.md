# Tout ce qu’il ne faut pas faire sur un SI en entreprise…

Conférence donnée lors de l'événement Passage En Seine du 27 au 30 juin 2019 https://passageenseine.fr/

## Résumé

Tout ce qu’il ne faut pas faire sur un SI en entreprise… Et bien sûr ce que personne ne fait…

Dans une volonté de pédagogie, de partage et d’amélioration des connaissances dans les bases de la sécurité d’un système informatique d’entreprise (SI), petit tour des mauvaises pratiques ou absence de pratiques que l’on retrouve sur les SI d’entreprises.

Sur fond de rappel des bonnes pratiques d’hygiène numérique (Beaucoup de règles évidentes mais si cette conférence existe…).

Public visé : du technicien de support à l’administrateur système et réseau, du DSI en passant par le RSSI, du développeur au devops, toute personne un tant soit peu curieuse.

Conférence non technique et généraliste

# Command line to create a pdf from the source

```
#!/bin/bash
pandoc -t beamer Genma_PSES2019_Pas_faire_SI.md -o Genma_PSES2019_Pas_faire_SI.pdf --slide-level=2 --dpi=300
```

# Licence

CC-BY-SA

