% \#PSES2019 - Tout ce qu'il ne faut pas faire sur un S.I. en entreprise... Et bien sûr ce que personne ne fait...
% Alexander Wayne
% Choisy le Roi - Passage en seine, 30 Juin 2019

# Note sur la présentation

## Résumé

Dans une volonté de pédagogie, de partage et d'amélioration des connaissances dans les bases de la sécurité d'un système informatique d'entreprise (S.I), petit tour des mauvaises pratiques ou absence de pratiques que l'on ne voit JAMAIS sur des machines en production.

## Public visé

Du technicien de support à l'administrateur système et réseau, du D.S.I en passant par le R.S.S.I., du développeur au devops, toute personne un temps soit peu curieuse. Conférence non technique et généraliste.

# Cette présentation aurait pu aussi s’appeler *Le petit guide d'hygiène numérique à destination des D.S.I.*

## Beaucoup de règles semblent et sont évidentes

Mais si la conférence existe, c'est bien parce que comme dit le titre "personne ne fait".

## Ce *personne ne fait* est à double sens :

* personne ne fait : car il **ne faut pas** faire ;
* personne ne fait : et pourtant il **faudrait** faire.

# Qui je suis ?

## Alexander Wayne ???

* Certain.e.s d'entre vous m'ont reconnu.
  * Expérience : vient-on pour moi ou pour mes sujets de conférence ?
  * Un autre pseudonyme du fait du *disclaimer*.

## Les métiers que j'ai exercés

J'ai *quelques* années d'expériences au-cours desquelles j'ai exercé plusieurs métiers (parfois en même temps) dans l'informatique. A savoir :

* Technicien de support 1 à 3
* Consultant, Chef de projet
* Développeur dans différents langages
* Administrateur système et réseaux
* D.S.I. & R.S.S.I.

# Disclaimer

## Ou le dégagement de responsabilité

* Cette conférence est issue de mon expérience personnelle, à travers les différents métiers que j'ai pu exercé.
* **Aucune entreprise et aucun client ne seront cités.**
* Dans toutes les entreprises et clients chez qui j'ai travaillé, le S.I. était à l état de l'art. Et j'ai donc appris ainsi toutes les bonnes pratiques ! #Troll

# C'est parti

![Sortez vos bullshit bingo](./Over9000.jpeg)

# Quelques acronymes

## Les acronymes

* S.I. : Système Informatique, Système d'information
* D.S.I. : Directeur du Service Informatique
* R.S.S.I.: le Responsable de la Sécurité Informatique du S.I.
* Collaborateurs.trices : les employé.e.s de l'entreprise

# L'argent

## Règle d'or

* Le S.I. interne est un centre de coût - pas de revenus.
* Donc on met le point d'argent possible dedans.

# Les mises à jour

## Ne jamais faire les mises à jour

* **Règle d'or : Si ça marche, on ne touche pas.**
* Et conséquence indirect : ça économise de la bande-passante...

# La documentation

## Règles concernant la documentation

* On ne fait pas de documentation car c'est une perte de temps, ça demande de faire des mises à jours...
* De toute façon, elle ne sera pas maintenue...

# Supervision

## Ne pas traiter les Warning

* Dans la supervision, si ce n'est pas en rouge, ce n'est pas important. Donc on traitera plus tard.

## L'astuce

* La meilleure technique de supervision reste **La supervision à l'oreille**.
* On coupe le serveur (plus propre *on suspend la VM ou on débranche un câble réseau*) et on attend de voir au bout de combien de temps il y a une réclamation... **Pas de nouvelles, bonnes nouvelles**.

# Supervision - on n'est jamais trop informé

## Gestion des alertes

Toute remontée d'alerte donne lieu à :

* l'envoi d'un mail
* l'envoi d'un SMS
* un message sur le chan IRC, Slack par un bot

Petit bonus : un son d'alarme.

# Sécurité - Les mots de passe

## Stockage des mots de passe

* Les mots de passes sont stockés en clair dans la base de données et accessible via une simple requête SQL (donc non chiffré, non salé)
  * C'est plus pratique quand un collaborateur a oublié son mot de passe.

## Politique des mots de passe

* Taille, format et renouvellement régulier imposé
  * 12 caractères maximum, avec au moins une majuscule, 1 chiffre, le mot de passe devant être différent des 25 derniers mots de passe et changés tous les mois...

# Sécurité - Les mots de passe et le serveurs

## Root a le même mot de passe sur toutes les machines

* Comme ça on n'a pas de problème avec les accès.

# Sécurité - Accès distant

## Connexion à distance

* On autorise les connexions SSH à l'utilisateur Root. Si l'admin sait faire une connexion SSH, il sait ce qu'il fait.

## Ne pas avoir de traçabilité des actions

* Car un hacker qui pownerait la machine aurait alors une source d'information précieuse sur ce qui tourne sur la machine via les commandes shell précédemment lancées.

# Réseau - avoir un réseau sécurisé

## Proxy

* Il faut bloquer StackOverflow et autres Reddit, les développeurs
* Il faut bloquer *les dépôts* Github, Docker, NPM etc.

Les utilisateurs vont tout faire pour contourner ces limitations et cela induit des gros problèmes de sécurité.

## Faire du Man in the middle (SSL/TLS)

* Les connexions cryptées c'est le mal et il faut bien surveiller les utilisateurs (sans les en informer sinon ils vont râler).

# Réseau - avoir un réseau moderne

## Un réseau Open-bar

* Sans aucun filtre, sans surveillance... pour le confort des collaborateurs (Des vidéos HD de YouTube en musique de fond toute la journée...)

# Réseau - le matériel

## Un réseau le plus simple possible

* Tout sur le même réseau.

## Un réseau le plus complexe possible

* On met en place pleins de VLAN, de DMZ pour sécuriser au maximum.

## Mise à jour et paramétrage des routeurs

* Rappel : si ça marche, on ne touche pas.

# Réseau - le navigateur

## Le navigateur par défaut

On proposera deux navigateurs par défaut sur les postes utilisateurs :

* Chrome, parce que tout le monde veut *Google* sur son ordinateur ;
* Internet Explorer 6, pour garder la comptabilité avec l'intranet.

# Matériel - les serveurs

## Le spare

* Les machines ça coûte cher et avec la virtualisation on fait tourner plusieurs V.M. sur **1** serveur.

## Le câblage de la salle serveur

* Il ne faut pas perdre de temps avec le câblage à faire de belles baies biens brassées, de toute façon rapidement ça va être le bazar. Autant que ça le soit de suite. Gain de temps. Et donc d'argent.

# Matériel - les serveurs

## Une gestion de noms

* On nomme tous les serveurs selon des codes / une nomenclature bien précise, mais obscure (on évite d'appeler le serveur de compta "Serveur de la compta", ça aide les hackers)
* Les noms des planètes de Star Wars ou Serveur1, Serveur2, Serveur3 c'est bien.

# Matériel - les serveurs

## Pas de plateforme de recette

* Ça coûte de l'argent
* On est AGILE ou on l'est pas ?

# Matériel - les PC des collaborateurs

## Attribuer du matériel toujours plus performant

* Il faut anticiper les demandes du type "J'ai 8go de RAM, j'ai besoin d'un portable avec 16go de RAM pour accélérer mes développements". ACCELERER...  (#Troll pour accélérer, il faut un SSD et un processeur qui va plus vite c'est bien connu...)
* Un développeur sachant forcément ce qu'il fait et pourquoi il a besoin de 16 ou 32 Go de RAM pour faire un site Internet statique, on peut lui faire confiance.

# Matériel - les PC des collaborateurs

## Byod - Bring Your Own Device

* Les stagiaires apportent leur propre PC et le branche sur le S.I.

## Les mises à jours des postes des collaborateurs

* Tout le monde a les droits root sur sa machine.
* Les mises à jour et installation de programmes sont gérés par les utilisateurs. Cela évite d'avoir recours à de la télémaintenance.

# Matériel - les PC des collaborateurs

## Gestion

* Il ne faut pas perdre de temps avec une estimation des coûts d'amortissement et planifier le renouvellement des machines.
* Les machines doivent tourner jusqu'à ce qu'elles meurent.

## Pas de suivi de l'état du parc matériel

* On aura bien une demande de PC quand des nouveaux collaborateurs arriveront et on verra à ce moment là.

# Les sauvegardes

## Pour les sauvegardes, il y a le cloud

* Le cloud, c'est espace de stockage illimité...

# Ne jamais faire de ménage

## Aka : on ne sait jamais, ça peut toujours servir.

* Il faut laisser des tas de fichiers de la forme nom_fichier_conf_YYYMMAA. Ca fait une sauvegarde, ça permet de garder l'historique de configuration...
* Il faut laisser les logs (pas de rotations des logs) au cas où on en a besoin

# R.H. et S.I.

## Moins on met de collaborateur, mieux c'est

* Le S.I. est un centre de coût (et ne rapporte pas), on mettra donc le minimum de ressources.
* Si vraiment il faut des ressources, on pourra éventuellement mettre des stagiaires et des alternants qui vont apprendre plein de choses.

# R.H. et gestion des collaborateurs

## On est débutant mais sur le papier on est expert

* Un débutant est vendu comme expert aux clients : il en sait toujours plus que le client, c'est d'ailleurs pour ça que le client paie.
* Il ne faut jamais prendre en compte les avis de celleux qui ont la connaissance et l'expertise. Un expert ça coûte cher et il va mettre en place des solutions compliquées qui feront que seulement lui pourra intervenir...
* Si besoin, Google donnera les réponses.

## Solidarité entre collaborateurs

* On sollicite les collaborateurs qui sont chez des clients pour faire une autre mission en parallèle pour un autre client.

# R.H. - S.I. - Gestion des ressources

## Rappel : le S.I. est un centre de coût

* Le DSI est aussi le RSSI et est aussi le technicien support niveau 1.
* Les administrateurs systèmes font les installations des PC, le support niveau 1 aux utilisateurs.

# R.H. - S.I. - Départ des collaborateurs

## Partir un jour... sans retour...

* Ne pas faire le ménage des comptes, des droits, des accès...
* Et surtout ne jamais faire d'audit des machines à ce sujet, là encore c'est une perte de temps.

*Cela fera gagner du temps quand le collaborateur reviendra car il aura constater son erreur, non ce n'est pas mieux ailleurs...* #Troll

# Gestion de crises

## On verra bien le moment venu, ça ne sert à rien de perdre du temps avec ça...

* Ne pas avoir de procédures de PRA, PCA, de gestion de crises ;
* Personne n'est formé et ne prend le pilotage / coordination quand une crise se présente ;
* On ne prend pas le temps de communiquer régulièrement avec le client et on le laisse dans le noir : on a autre chose à faire, bah oui on est en gestion de crise.

# Le D dans D.S.I

## Veut dire directeur... sur le papier

* *La direction* ne doit pas écouter les recommandations et ne pas soutenir politiquement son DSI / RSSI.
* De toute façon avec un DSI / RSSI, c'est toujours "C'est compliqué, ça va prendre du temps, ça va coûter cher...".

# RGPD, le fameux RGPD

## Comme on respecte le RGPD, on ne fait pas d'envoi des mails massivement

* Et encore moins en se basant sur une liste d'adresses récupérées au fur et à mesure des années.
* On peut donc laisser les personnes utiliser le serveur SMTP sortant pour envoyer des mails sans surveillance ou analyse du trafic sortant.
  * Tout nouveau serveur disposera d'ailleurs d'un service Postfix configuré en relais, car ça peut toujours servir.

# Les sites Webs

## Une fois qu'un site a été développé

* On ajoute quand même des nouveaux plugins pour enrichir les fonctionnalités
* On ajoute des nouveaux comptes

## Si ça marche on ne touche pas

* Wordpress, Drupal, ça marche bien donc une fois que c'est fini, on ne touche plus.

# Sécurité et les sites Webs

## Les développeur savent ce qu'ils font

* On dit bien *DevOps* : si ille met en production, c'est qu'ille a fait du code qui marche.

# Conclusion

## Un guide d'hygiène numérque de l'administrateur système ?

* Beaucoup de règles évidentes mais si cette conférence existe c'est parce que comme dit le titre "personne ne fait".
* Il faudra(it) que je reprenne cette conférence pour faire ce fameux guide.

# Questions ?

## Rappel du disclaimer

* **Aucune entreprise et aucun client ne seront cités.**
  * **Dans toutes les entreprises et clients chez qui j'ai travaillé, le S.I. était à l état de l'art. Et j'ai donc appris ainsi toutes les bonnes pratiques !** #Troll
