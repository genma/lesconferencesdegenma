% Why the Opensource Center by Atos is developping a business model around Nextcloud ?
% Jérôme HERLEDAN - ATOS
% Nextcloud Conference 2019 - Berlin

## About me

![](./images/avatar.png)

### Also known as Genma

 * French Blogger *Le blog de Genma* https://blog.genma.fr
 * Twitter : @genma
 * Mastodon : genma@framasphere.org
* **Nextcloud user (Evangelist ?)**

### And actually

* I work for Atos a Project Manager, Architect and Opensource specialist

# Atos

## Atos

### French multinational information technology (IT) service and consulting company 

Headquartered in Bezons, France and offices worldwide. 

* It specialises in :
  * hi-tech transactional services, 
  * unified communications, 
  * cloud, 
  * big data 
  * and cybersecurity services.
 * Atos operates worldwide under the brands Atos, Atos-Syntel, Atos Consulting, Atos Healthcare, Atos Worldgrid, Bull, Canopy, Unify and Worldline. 

## AOSC

### Atos Open Source Center

* A center of expertise and support for Open Source software (Red Hat and Community) by Atos.
* We are Red Hat partner.

## Offers and services

### Consulting : advice and expertise

* To evaluate the opportunity and / or the impacts of using or migrating to Open source as well as to help in the choice of Open source components 
 * From assistance to implementation

### Support

* Survey and avise to our customers around the Opensource software they used
* Hotline / Helpdesk
* Bugs corrections

## Offers and services

### Devops Factory

* Devops based on Opensource software for our internal needs and customer needs

### Digital Transformation

* Micro-services architecture
* Iaas, Paas, Caas
* Cloud : private, hybdrid, public
# Atos and Nextcloud ?

# Why the Opensource Center by Atos is developping a business model around Nextcloud ?

## Because of me !

As said, I'm an Opensource specialist and above all a Nextclouders.

I really believe (and know it's true) that's Nextcloud is a response to GAFAM (USA) or Chinese cloud. And not only me !

## Nextcloud popularity increased !

### EU governments choose independence from US cloud providers with Nextcloud

 * French Ministry of Interior is now preparing a roll-out of a production-ready Nextcloud to their users, designed to scale to the 300.000 employees of the ministry.

https://nextcloud.com/blog/eu-governments-choose-independence-from-us-cloud-providers-with-nextcloud/

# So why, Atos ?

## Because

Atos is a **VERY** big company.

* We have many thousand of collaborators from all expertise.
* We are able to respond to all the customers needs.

## What we plan to do with Nextcloud ?

### Just do what we know to do with Opensource

* Convince them to use Nextcloud as an alternative for their *cloud* needs.
* Install them Nextcloud on their infrastructure or host for them Nexctloud servers on our cloud offer / datacenter.
* Provided them support.

**Let's create a Nextcloud offer for or customer.**

## Benefits for Nextcloud ?

* More customers and users
* Bug Reports
* Contributions
  * New apps
  * Existing apps improvement

# Next

## What's next

### Find customers

* Finish to construct the offer
* ... and communication about it

### Partership

* With Nextcloud
* With Collabora

### Meetup

We planed to organize a Nextcloud Meetup on Paris, France
Status : Work In Progress

# Thanks for your attention
