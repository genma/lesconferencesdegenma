# Why the Opensource Center by Atos is developping a business model around Nextcloud ?

Lightning talk done during the Nextcloud Conference 2019, Berlin, September 14-15 https://nextcloud.com/conf-2019/

## Short Abstract

The OpenSource Center by Atos, located in Paris, France has for goals to developp expertise on Opensource components and to capitalize our feedback on the implementation of solutions based on Free Software in the major systems integration projects of Atos. Has a new collaborator, being a Nextclouders, I work on developping a business & partnership around Nextcloud & Collabora for our customers. That's my feedback.

# Command line to create a pdf from the source

```
#!/bin/bash
pandoc -st beamer -V theme:Custom -V lang:fr-FR Atos_Opensource_Center_Nextcloud.md -o Atos_Opensource_Center_Nextcloud.pdf --slide-level=2 --dpi=300
```

# Licence

CC-BY-SA
