# Conférence Guide d'hygiène numérique version 2020

Remise à jour du viellissant "Guide d'hygiène numérique". Version 2020, avec ajout de quelques slides issus de mes conférences "Degooglisons Internet" pour Framasoft.

Le support est disponible dans ce dépôt.

# Commandes pour créer un pdf à partir des sources

```
#!/bin/bash
pandoc -st beamer -V theme:Custom -V lang:fr-FR Genma_Petit_Guide_d_hygiene_numerique.md -o Genma_Petit_Guide_d_hygiene_numerique.md.pdf --slide-level=2 --dpi=300
```

# Licence

CC-BY-SA
