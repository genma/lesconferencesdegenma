% Guide d'hygiène numérique
% Version 2020
% Genma

# Préambule

##  ![](./images/accessibilite_v2.jpg){width=250px} Accessibilité

### Visuelle

* Est-ce que tout le monde arrive à lire la présentation ?
* Elle est disponible en ligne, pas la peine de prendre de notes :-)

### Auditive

* Je parle vite. Très vite.
* Merci de me faire signe pour me demander de ralentir, d'articuler.

## A propos de moi - Genma

![Mon avatar](./images/avatar.png)

* **Un blog :** https://blog.genma.fr
* **Twitter :** @genma
* **Membre de Framasoft**

**Je parle d'hygiène numérique depuis quelques années.**

# De quoi allons-nous parler? 

## Deux parties

### Hygiène numérique et des règles basiques

### Vie privée sur Internet & données personnelles 

# Un peu d'hygiène numérique

## L'hygiène numérique ?

![](./images/nettoyer_son_clavier.jpg)

## L'hygiène numérique !

::: {.block}
### Une définition?

* L'hygiène est un ensemble de mesures destinées à prévenir les infections et l'apparition de maladies infectieuses.
* L'hygiène numérique, ce sont des règles destinées à mieux utiliser son ordinateur, en sécurité, de façon simple.
:::

**L'hygiène numérique c'est comment éviter la *gastro informatique*.**

## Un exemple

### On me vole mon PC

[](./images/laptopthief.jpg)

*  Quelles sont les données que je perds ? 
   * Ce qui amène la notion de **sauvegarde**.
*  Quelles sont les données que l'on trouve ?
   * Ce qui amène la notion de **chiffrement, de coffre-fort numérique**.

## Sauvegarde simple et efficace ?

 ![](./images/backup.jpg){width=450px}

### Le disque dur externe

* Méthode simple : copier-coller.  
* Méthode plus avancé : on "synchronise".
* On le dépose chez un ami, un voisin, un parent (pour éviter le vol, l'incendie...)

*Petit plus : chiffrer le disque pour plus de confidentialité.*


## Coffre-fort numérique ?

L'option de chiffrement des appareil ou via un logiciel dédié (Veracrypt).

![](./images/coffre_fort_numerique.png)

## Crypté ? Cryptage ?

![](./images/chiffrees_vs_cryptees.png)

# Les mots de passe

## Les mots de passe

### Règles

* Plus c'est long, plus c'est bon.
* Ne pas avoir le même mot de passe pour deux comptes en ligne.
* Passer à des *phrases de passe* (technique des dés...).

### Les sites permettant de tester ses mots de passe ?

* Ils sont la meilleure façon de constituer une base de données de mots de passe.
* Ne pas tester son vrai mot de passe mais un mot de passe du même type/de la même forme.
* **Les mots de passe sont personnels**.

## Trop de mot de passe à retenir?

Il y a des coffres-forts numériques de mot de passe.

### KeepassX 

Le coffre-fort numérique des mots de passe

![](./images/keypassX.png)

## Gestion des comptes

### Des comptes pour des usages différents

* Créer un compte utilisateur et un compte administrateur.
* Au quotidien, utiliser le compte utilisateur.

### Le compte administrateur ?

* Il porte bien son nom, il ne doit servir qu'aux tâches d'administration (installation des logiciels...)
* Quand l'ordinateur pose une question "Je peux lancer ce programme ?", réfléchir. Ne pas dire oui tout de suite.

## Mises à jour de sécurité

### FAIRE LES MISES A JOUR

* Avoir un système à jour.
* Avoir des logiciels à jour.
* Avoir un antivirus à jour.

### Les logiciels ont des bugs

* Un bug peut-être utilisé par un virus...
 * Mettre à jour, c'est corriger les bugs, donc se protéger.

##  Sur Ordinateur, installation de logiciels

### Logiciels payants - propriétaires

* Pas de logiciels crackés
* Pas de téléchargement de logiciels depuis un autre site que le site officiel.
On oublie les sites 01Net, Télécharger.com qui remplacent le navigateur par Chrome...
* Que les logiciels dont on a besoin (pas de démos, de logiciels marrants...)

### Logiciels libres

* Préférer le logiciel libre - open source.
* Passer par l'annuaire de Framasoft : https://framalibre.org/.

## Framalibre.org

![](./images/framalibre.png)


##  Le copain qui s'y connait

### Attention

* Ne pas le laisser installer les logiciels *crackés*, *piratés*, source de virus.
* Chercher à comprendre ce qu'il fait, lui demander. 
* S'il n'est pas capable d'expliquer, se méfier. Voire refuser.

### PC = Personal Computer

* Ne pas faire confiance. Il ne faut pas prêter sa machine sans voir ce que fait l’individu à qui vous l’avez confiée.
* Il faut prévoir une session invitée. 
* Il est si facile d’installer un virus sur un PC... Méfiez-vous de ce que l'on fait sur votre PC.

# En appliquant, ces règles, on a tout de suite beaucoup moins de soucis avec son PC.

# Hygiène numérique & Internet

# Toutes ces traces qu'on laisse sur Internet... sans le savoir. Les données qui sont prises à notre insu... Comment est-on suivi à la trace sur Internet?

# Cloud - l'informatique dans les nuages

## Définition du cloud

* Le **Cloud, c'est l'ordinateur d'un autre.**

![](./images/cloud_data_center.jpg)

# Les GAFAMs

## Les GAFAM

![](./images/gafam.jpg)

## Les GAFAM

### GAFAM : Google, Apple, Facebook, Amazon, Microsoft
* Concentration des acteurs d’Internet autour de silos ;
* Une centralisation nuisible (frein à l'innovation) ;
* Les utilisateurs de ces services ne contrôlent plus leur vie numérique.

## Les GAFAM

![](./images/gafam_silo.png)

## Les GAFAM en bourse

![](./images/gafam_bourse.png)

## Google au début

![](./images/google_debut.png)

## Google devenu alphabet

![](./images/google_now.png)

# Sur Internet, si c'est gratuit, c'est VOUS le produit

## Comment est-on pisté ?

![](./images/Facebook_like.png){width=250px}

### Toutes les publicités nous espionnent

* Le bouton Like de Facebook : il permet à Facebook de savoir que vous avez visité ce site, même si vous n'avez pas cliqué sur ce bouton. Et ce même si vous vous êtes correctement déconnecté de Facebook.
* Tout ce qui est fait avec Google...
* Tous les publicités, 
* Amazon...


# Hygiène numérique \& Internet

## Choisir le bon navigateur

![](./images/Firefox.jpg)

## La navigation en mode privé

### Quelles données ne sont pas enregistrées durant la navigation privée ?

* pages visitées ;
* saisies dans les formulaires et la barre de recherche ;
* mots de passe ; 
* liste des téléchargements ; 
* cookies ;
* fichiers temporaires ou tampons.

## Des services fournis par Firefox

![](./images/Firefox_services.png)

## Firefox Monitor

![](./images/firefox_monitor.png)

## Firefox Lockwise

![](./images/firefox_lockwise.png)

## Installer des extensions

![](./images/extensions_firefox.jpg)

## Ublock Origins - Bloquer les publicités

![](./images/Adblock01.png)

## Ublock Origins - Bloquer les publicités

![](./images/Adblock02.png)

## Ghostery, Privacy Badger, Noscript...

Bloquer tous les trackers associés au site.

![](./images/Ghostery_tracker.png)

# Changer de moteur de recherche

![](./images/dontbeevil.jpg)

## Duckduckgo - https://duckduckgo.com

![](./images/DuckDuckGo.jpg)

## Qwant - https://qwant.com

![](./images/Qwant.jpg)

# Changer de Cloud ?

## Le cloud : la malbouffe de l'informatique

![](./images/malbouffe.png)

## Framasoft et tous ses outils de Degooglisons

![](./images/framasoft_degogglisons.jpg)

## Framasoft et tous ses outils de Degooglisons

![](./images/degooglisons_01.png)

## Framasoft et tous ses outils de Degooglisons

![](./images/degooglisons_02.png)

## Les CHATONS

![](./images/chatons.png)

## Les CHATONS

![](./images/chatons_02.png)

# Pour vous aider & apprendre

## Le monde associatif

![](./images/monde_associatif_01.png)

## Agenda du libre

![](./images/monde_associatif_02.png)

# La fin ?

## Le site https://www.hygiene-numerique.com/

![](./images/site_hygiene-numerique.com.png)

# Merci de votre attention
