% Yunohost & Nextcloud
% The selfhosted cloud for a better privacy
% Genma - Nextcloud Conference 2019 - Berlin

## About me - Genma

![Mon avatar](./images/avatar.png)

* French Blogger *Le blog de Genma* https://blog.genma.fr
 * Twitter : @genma
 * Mastodon : genma@framasphere.org
* Member of **Framasoft** (*Degooglize Internet*, Chatons...)
* FreeSoftware user since 2003
* **Selfhosted with Yunohost**
  * User and member of the french community
* **Nextcloud user (Evangelist ?)**

# Yunohost

## Yunohost

* The operating system to build the decentralized Internet
  * The Internet cube used Yunohost as O.S.

Kind of **"The Ubuntu of self-hosting"**

**https://yunohost.org**

## Yunohost - Features overview

Yunohost Basics :

* Debian-based (stable, robust, well-known)
* Simple & clean web administration interface
* Install apps in just a few clicks & questions
* Multi-users with single sign-on (SSO) portal
* Email and instant messaging (XMPP) out of the box
* Backups (and restore!)
* Let's Encrypt integration
* Firewall, fail2ban, ...

**Lots of other stuff for things to just work**

## Yunohost - Architecture

![](./images/Yunohost_Architecture.png)

## Yunohost - Login interface

![](./images/Yunohost_login.png)

## Yunohost - User interface

![Yunohost -](./images/Yunoshost_User_apps.png)

## Yunohost - Web admin interface

![](./images/Yunohost_Interface_admin.png)
 
## Yunohost - Web vs command line

![](./images/Yunohost_Webinterface_vs_command_line.png)

## Yunohost - Apps are packaged

All applications are "packaged" and available on a Github repository.
* Shell scripts with a Yunohost "moulinette"

![](./images/Yunohost_Apps_Package.png)

# Nextcloud & Yunohost

## Nextcloud & Yunohost

* Install Nextcloud ... in a few clicks !
* As many apps, Nextcloud is packaged 
   * https://github.com/YunoHost-Apps/nextcloud_ynh

## Nextcloud & Yunohost - In Background - A shell script

![](./images/Yunohost_Script_install.png)

## Nextcloud & Yunohost - Graphical installation 

![](./images/Yunohost_Nextcloud_Installation_01.png)

## Nextcloud & Yunohost - Graphical installation 

![](./images/Yunohost_Nextcloud_Installation_02.png)

## Nextcloud & Yunohost

![](./images/Yunohost_Nextcloud02.png)

# Interessed ? Convinced ?

## How to help Yunohost

* Deploy a YunoHost for yourself ;)
* Tell your friends, organize install parties
* Give us feedback on the UX
* Report issues, test apps and beta releases
* Improve doc and translations

... and if you know how to code:

* Front-end (HTML / CSS / JS)
* App packaging (bash)
* Core (python, general sysadmin knowledge, security, ...)

**https://yunohost.org**

# Thanks for you attention
