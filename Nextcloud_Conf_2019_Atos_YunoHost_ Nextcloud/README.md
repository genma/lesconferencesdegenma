# YunoHost & Nextcloud, the selfhosted cloud for a better privacy

Lightning talk done during the Nextcloud Conference 2019, Berlin, September 14-15 https://nextcloud.com/conf-2019/

## Short Abstract

YunoHost is a server operating system aiming to make self-hosting accessible to as many people as possible, without taking away from the quality and reliability of the software. Among all the available apps, there's Nextcloud. Easy to install, easy to manage, why you no host your own Nextcloud ?

# Command line to create a pdf from the source

```
#!/bin/bash
pandoc -st beamer -V theme:Custom -V lang:fr-FR Genma_YunoHost_Nextcloud.md -o Genma_YunoHost_Nextcloud.pdf --slide-level=2 --dpi=300
```

# Licence

CC-BY-SA
